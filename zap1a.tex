% Začátek preambule
\documentclass[a4paper,titlepage,final,12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[czech]{babel}
\usepackage{csquotes}
\usepackage[top=2.5cm, left=2.5cm, text={16cm, 24cm}, ignorefoot]{geometry}
\usepackage{makeidx}
\usepackage[backend=biber, style=authoryear, hyperref=true]{biblatex}
\usepackage[unicode]{hyperref}
\addbibresource{biblio.bib}
\hypersetup{breaklinks=true}
\renewenvironment{theindex}
               {\section*{\indexname}
                \thispagestyle{plain}\parindent\z@}
               {}
\makeindex

\author{Stanislav Marek}
\title{Iterační výpočty}

% Konec preambule

% Začátek dokumentu
\begin{document}

\section{Úvod} \label{uvod}
  Iterační výpočty patří mezi základní nástroje každého programátora v~jazyce~C. Počítání zadaných matematických funkcí je z~hlediska algoritmizace velmi vhodným procvičením těchto technik.

  Tento dokument popisuje princip implementace zadaných matematických operací. Program vytvořený pro tento účel se spouští v~konzoli, a~to~s~parametrem (či více parametry), který určuje vypočítávanou matematickou operaci. Data čte ze standardního vstupu a~výsledek vypisuje na zadaný výstup v~předem zadaném formátu.

  Dokumentace popisuje celý proces návrhu a~implementace algoritmů pro výpočet zadaných matematických funkcí. Čtenář se v~kapitole \ref{analyza} dozví informace o~analýze a~návrhu řešení, v~kapitole \ref{problematika} se dozví teorii nutnou ke~správnému sestavení algoritmů a~v~kapitole \ref{testovani} navrhované testovací hodnoty. 
\section{Analýza a návrh řešení zadaného problému} \label{analyza}\index{Analýza a návrh řešení}

  Návrh tohoto programu není triviální záležitostí. Je nutné ovládat matematiku a~správně pochopit operace, jež požadované matematické funkce provádí.

  \subsection{Stručné shrnutí zadání}\index{Analýza a návrh řešení!Shrnutí zadání}
    \begin{itemize}
     \item Implementujte funkci \index{Arcus sinus}arcus sinus $arcsin(x)$ počítanou na~zadaný počet platných číslic pomocí taylorovy řady \footcite{planetmath}
     \item Implementujte funkci \index{Logaritmus}logaritmus $log_a(x)$ počítanou se~zadaným základem a přesností na~zadaný počet platných číslic
     \item Implementujte výpočet délky lomené čáry bez chyby a s chybou průběžně vypisující délku lomené čáry z~již zadaných hodnot
     \item Při implementaci nesmíte v~iteračních výpočtech používat knihovní funkce z math.h\footcite{creference}
    \end{itemize}

\noindent Pro účely dalších výpočtů jsem si definoval funkce:
    \begin{itemize}
     \item \index{Mocnina} \verb!mocnina(mantisa, exponent)! -- Pomocí cyklu vypočítám mocninu libovolného základu s~libovolným exponentem
     \item \index{Absolutní hodnota} \verb!absolut(cislo)! -- pokud je \verb!cislo! záporné, vrátí jeho opačnou hodnotu
     \item \index{Platné číslice} \verb!platnaCislice(sigdig)! -- vypočítá epsilon pro výpočet s~přesností na zadaný počet platných číslic
     \item \index{Arcus Tangens} \verb!arctan(cislo, epsilon)! -- vypočítá arcus tangens pro zadané číslo na zadaný počet platných číslic
     \item \index{Přirozený logaritmus} \index{Logaritmus (přirozený)} \verb!logarithmnatural(cislo, epsilon)! -- vypočítá přirozený logaritmus pomocí taylorovy řady pro logaritmus, pro různé intervaly používá různé výpočty, počítá s přesností na zadaný počet platných číslic
    \end{itemize}

\noindent Výše popsané funkce (podprogramy) jsou použity ve výpočtech matematických funkcí zadaných v~zadání projektu. 

\section{Možné problémy při implementaci}\label{problematika}\index{Problémy při implementaci}
  Vzhledem k~tomu, že při výpočtech používáme nekonečné řady, může nastat několik problémů:

  \subsection{Velká časová náročnost}\label{narocnost}\index{Problémy při implementaci!Časová náročnost}
    Pokud správně neurčíme množství průchodů potřebných pro dostatečně přesný výpočet dané funkce, bude náš program zbytečně pomalý. To se dá vyřešit buď nákupem rychlejšího stroje, který náš program provádí, nebo optimalizací výpočtu.
    
    U~funkce arcus sinus sice existuje taylorova řada pro jeho výpočet, ale její použití je krajně nevhodné, neboť pro zadanou přesnost je třeba udělat příliš mnoho matematických operací v~přílišném počtu opakování. Proto je efektivnější zjistit jiný způsob výpočtu funkce arcus sinus. Já jsem na radu našich profesorů zvolil vztah\footcite{mathonweb}: $arcsin(x) = \frac{\sqrt{1 - x^{2}}}{x}$.

  \subsection{Nesprávné určení definičního oboru}\index{Problémy při implementaci!Nesprávné určení definičního oboru}
    Definiční obor je nutné znát pro testování při průběhu programu. Nesmíme dovolit, aby se do algoritmu pro výpočet dostalo číslo, které nepatří do definičního oboru vypočítávané funkce.
    Např. při implementaci funkce obecného logaritmu pomocí přirozených logaritmů musíme brát v~úvahu definiční obor přirozeného logaritmu a~přizpůsobovat mu výpočet. Různé intervaly definičního oboru přirozeného logaritmu mají různé taylorovy řady pro výpočet jeho hodnoty.

  \subsection{Příliš malá přesnost výpočtů aneb jak jsem implementoval epsilon}\index{Problémy při implementaci!Nedostačující přesnost}\index{Epsilon}\index{Platné číslice}
    Musíme vědět, co že je ta zadávaná přesnost na platné číslice a~musíme s~ní správně pracovat. Nejprve si vypočítám řád do \verb!epsilonRad!, který může tato přesnost ještě ovlivnit. Poté porovnávám rozdíly v průchodu cyklů tím způsobem, že od nové hodnoty odečtu hodnotu starou a~výsledek porovnám s~relativní odchylkou epsilon vypočítanou vynásobením \verb!epsilonRad! a~proměnné, do které se ukládá mezisoučet všech doposud vypočítaných hodnot. Pokud je výsledek větší nebo roven epsilon, je splněna podmínka pro ukončení cyklu.

  \subsection{Špatná vstupní data}\index{Problémy při implementaci!Špatná vstupní data}\index{EOF}\index{End Of File}
    Pokud chcemep očítat s~čísly, měli bychom si dát pozor, abychom s~nimi opravdu počítali. Provedeme tedy kontrolu vstupních dat a všechny nevyhovující znaky přeskočíme. Toto funguje skvěle, ale cyklus je nekonečný, musíme tedy přidat podmínku pro ukončení. V zadání projektu se píše, že cyklus má skončit, narazí-li na konec souboru (EOF~--~End~Of~File).
    
\section{Testovaní funkčnosti}\label{testovani}\index{Testování}
  Pro testovací účely používám výpočty na webové stránce WolframuAlpha\footnote{\url{http://www.wolframalpha.com/}}

\section{Seznam literatury}  
 \printbibliography

\printindex

\end{document}
% Konec dokumentu